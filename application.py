#! /usr/bin/env python3

from db import *
from flask import Flask, render_template
app = Flask(__name__)
app.debug = True

@app.route('/')
def home():
    return render_template(
        "home.html")

@app.route('/user/')
@app.route('/user/<name>')
def user(name=None):

    db = DB()
    todo = db.get(name)

    return render_template(
        "user.html",
        name=name,
        todo=todo)

@app.route('/users/')
def users():
    db = DB()
    data = db.users()
    return render_template("users.html",data=data)

if __name__ == '__main__':
    app.run(host="0.0.0.0")
